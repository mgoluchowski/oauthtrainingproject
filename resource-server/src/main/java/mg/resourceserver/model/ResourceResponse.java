package mg.resourceserver.model;

public class ResourceResponse {

    private String text;

    public ResourceResponse() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
