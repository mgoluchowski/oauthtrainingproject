package mg.resourceserver.facade;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mg.resourceserver.model.ResourceRequest;
import mg.resourceserver.model.ResourceResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/res")
@RequiredArgsConstructor
@Slf4j
public class ResourceFacade {

    @GetMapping("/hardcoded")
    public ResponseEntity<ResourceResponse> getHardcodedData(@RequestParam String resourceId) {

        ResourceResponse resourceResponse = new ResourceResponse();
        resourceResponse.setText("Everything went fine.");
        return "2".equals(resourceId) ?
                new ResponseEntity<>(resourceResponse, HttpStatus.OK) :
                new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

}
