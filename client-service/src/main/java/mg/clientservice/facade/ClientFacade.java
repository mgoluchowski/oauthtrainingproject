package mg.clientservice.facade;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mg.clientservice.model.ClientResponse;
import mg.clientservice.service.GetResourceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/res")
@Slf4j
public class ClientFacade {

    private final GetResourceService getResourceService;

    public ClientFacade(GetResourceService getResourceService) {
        this.getResourceService = getResourceService;
    }

    @GetMapping("/hardcoded")
    public ResponseEntity<ClientResponse> getHardcodedData(@RequestParam String resourceId) {

        ClientResponse clientResponse = getResourceService.getResource(resourceId);

        return new ResponseEntity<>(clientResponse, HttpStatus.OK);
    }

}
