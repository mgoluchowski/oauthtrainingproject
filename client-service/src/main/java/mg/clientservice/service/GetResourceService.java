package mg.clientservice.service;

import lombok.RequiredArgsConstructor;
import mg.clientservice.client.ResourceServerClient;
import mg.clientservice.model.ClientResponse;
import mg.clientservice.model.ResourceResponse;
import org.springframework.stereotype.Service;

@Service
public class GetResourceService {

    private final ResourceServerClient resourceServerClient;

    public GetResourceService(ResourceServerClient resourceServerClient) {
        this.resourceServerClient = resourceServerClient;
    }

    public ClientResponse getResource(String resourceId){
        ResourceResponse resourceResponse = resourceServerClient.getResource(resourceId);
        ClientResponse clientResponse = new ClientResponse();
        clientResponse.setText(resourceResponse.getText());
        return clientResponse;
    }

}
