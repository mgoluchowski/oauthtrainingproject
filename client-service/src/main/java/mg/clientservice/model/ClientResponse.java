package mg.clientservice.model;

public class ClientResponse {

    private String text;

    public ClientResponse() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
