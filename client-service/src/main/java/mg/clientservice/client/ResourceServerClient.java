package mg.clientservice.client;

import lombok.RequiredArgsConstructor;
import mg.clientservice.model.ResourceRequest;
import mg.clientservice.model.ResourceResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ResourceServerClient {

    private final RestTemplate restTemplate;

    public ResourceServerClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResourceResponse getResource(String resourceId) {
        ResourceRequest resourceRequest = new ResourceRequest();

        String uri = "http://localhost:8999/res/hardcoded";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri)
                .queryParam("resourceId", resourceId);

        resourceRequest.setId(resourceId);

        ResponseEntity<ResourceResponse> resourceResponse =
                restTemplate.getForEntity(builder.toUriString(), ResourceResponse.class, resourceRequest);
        return resourceResponse.getBody();
    }

}
